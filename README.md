# TFCat examples and tutorials

This repository is providing examples and tutorials illustrating the usage of
 the TFCat specification and its associated python library.

## Tutorials
- [TFCat MultiPoint feature and feature processing](tutorials/MultiPoint_Example.ipynb)
  is showing how to use TFCat together with _shapely_ to process a _MultiPoint_ geometry 
  into a Polygon. 
- [TFCat MultiPolygon feature and data filtering example](tutorials/MultiPolygon_Example.ipynb)
  is showing how to use TFCat together with _shapely_ and _das2_ to use a 
  _MultiPolygon_ geometry to select data and so statistical analysis.
- [TFCat Multiple catalogue example](tutorials/Multiple_Catalogue_Example.ipynb)
  is showing how to compare TFCat features from different catalogues, using _shapely_ and 
  plot them on data with _das2_.

## Running the tutorials on Binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.obspm.fr%2Fmaser%2Fcatalogues%2Fexamples.git/main?labpath=tutorials)
