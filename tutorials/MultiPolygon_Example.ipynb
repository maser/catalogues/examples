{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e136f4f5",
   "metadata": {},
   "source": [
    "# TFCat MultiPolygon feature and data filtering example\n",
    "\n",
    "## Description\n",
    "The catalogue used in the example is available from _Wu et al (2022a)_. It contains Saturn Kilometric Radiation (SKR) bursts associated with an harmonic emission counterpart. The analysis is described in _Wu et al (2022b)_. \n",
    "\n",
    "Each feature has been identified by the study authors. The geometry type of the features is _MultiPolygon_. The features are composed of two sub-features: the first element of the _MultiPolygon_ geometry is the fundamental SKR burst emission contour, while the second element is that of the harmonic SKR emission.\n",
    "\n",
    "In this example, we process the first feature of the catalogue, process the geometries, load the data from a remote server, and filter the data using the geometry polygons. \n",
    "\n",
    "## References\n",
    "- Wu, Siyuan, P. Zarka, L. Lamy, U. Taubenschuss, B. Cecconi, S. Ye, G. Fischer __2022a__. _Catalogue of First Harmonic Saturn Kilometric Radiation Observations during Cassini’s Grand Finale_ (Version 1.0) [Data set]. MASER/PADC. https://doi.org/10.25935/T033-QS72\n",
    "- Wu, Siyuan, P. Zarka, L. Lamy, U. Taubenschuss, B. Cecconi, S. Ye, G. Fischer __2022b__. _Observations of the First Harmonic of Saturn Kilometric Radiation during Cassini’s Grand Finale_ submitted to GRL.\n",
    "\n",
    "## Requires\n",
    "- astropy\n",
    "- numpy \n",
    "- shapely\n",
    "- matplotlib\n",
    "- TFCat \n",
    "- das2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a6697cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "try: \n",
    "    from tfcat import TFCat\n",
    "except ModuleNotFoundError:\n",
    "    !{sys.executable} -m pip install tfcat\n",
    "    from tfcat import TFCat\n",
    "try:\n",
    "    import das2\n",
    "except ModuleNotFoundError:\n",
    "    !conda install --yes --prefix {sys.prefix} -c dasdevelopers das2py\n",
    "    import das2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6dd1a720",
   "metadata": {},
   "outputs": [],
   "source": [
    "from tfcat import TFCat\n",
    "import das2\n",
    "from astropy.time import Time\n",
    "import numpy\n",
    "from shapely.geometry import MultiPoint, Point, MultiPolygon, Polygon"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0960a816",
   "metadata": {},
   "source": [
    "### Loading the catalogue and display the geometry type of the first feature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d194f04b",
   "metadata": {},
   "outputs": [],
   "source": [
    "tf_file = '../data/catalogue.json'\n",
    "cat = TFCat.from_file(tf_file)\n",
    "cat.feature(0).geometry.type"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34a51101",
   "metadata": {},
   "source": [
    "### Extracting time range from the first feature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02678b73",
   "metadata": {},
   "outputs": [],
   "source": [
    "event = cat.feature(0)\n",
    "crs = cat.crs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b080acd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "tmin = crs.time_converter(event.bbox[0]).isot\n",
    "tmax = crs.time_converter(event.bbox[2]).isot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dbbe48b",
   "metadata": {},
   "source": [
    "### Using the _das2_ protocol to load data from a remote server\n",
    "\n",
    "The features have been identified in the Cassini/RPWS data collections, which are available from a _das2_ server managed by PADC (Paris Astronomical Data Centre). \n",
    "\n",
    "In this example, we retreive data from the Cassini/RPWS N3e data collection, which contains derived Stokes parameters from the observed radio waves. The _v_ (circular polarisation degree) variable is selected.   \n",
    "\n",
    "More information on _das2_: https://das2.org"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b1c0083",
   "metadata": {},
   "outputs": [],
   "source": [
    "src = das2.get_source(\"tag:das2.org,2012:site:/voparis/cassini-rpws-hfr/n3e/kronos_n3e_v/das2\")\n",
    "dataset = src.get( {'time' : (tmin, tmax, 4.0)} )[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b1c0084",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'Data of interest interval: {tmin} to {tmax}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8aaa4943",
   "metadata": {},
   "source": [
    "### Preparing the data for the filtering\n",
    "The filtering conducted as follows: the individual data record coordinates (time and frequency) are stored into a _MultiPoint_ object (`data_points`). In the `data_points` geometry object, we make use the third coordinate to store the number index of the data record within the dataset. This `data_points` geometry can then processed using intersection with the fundamental (`fund_polygon`) and harmonic (`harm_polygon`) geometries.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e469dfed",
   "metadata": {},
   "outputs": [],
   "source": [
    "das_times = Time(dataset['time']['center'][...].value).unix\n",
    "das_freqs = dataset['frequency']['center'][...].value\n",
    "\n",
    "coords = [(t, f) for t,f in zip(das_times.flatten(), das_freqs.flatten())]\n",
    "data = dataset['v']['center'][...].value.flatten()\n",
    "shape = dataset['v']['center'][...].value.shape\n",
    "data_len = len(data)\n",
    "index = numpy.arange(data_len, dtype=int)\n",
    "data_points = MultiPoint([Point(x, y, z) for (x, y), z in zip(coords, index)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ed8327f",
   "metadata": {},
   "outputs": [],
   "source": [
    "fund_polygon = Polygon(event.geometry.coordinates[0][0])\n",
    "harm_polygon = Polygon(event.geometry.coordinates[1][0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f53acfdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "fund_points = fund_polygon.intersection(data_points)\n",
    "print(f\"The fundamental SKR emission feature covers {len(fund_points.geoms)} data records\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2197672c",
   "metadata": {},
   "outputs": [],
   "source": [
    "harm_points = harm_polygon.intersection(data_points)\n",
    "print(f\"The harmonic SKR emission feature covers {len(harm_points.geoms)} data records\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79da7bae",
   "metadata": {},
   "source": [
    "### Creating a mask from the geometry filtering\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "df28b770",
   "metadata": {},
   "outputs": [],
   "source": [
    "mask_fund = numpy.zeros((data_len,))\n",
    "mask_fund[[int(geom.z) for geom in fund_points.geoms]] = 1\n",
    "mask_fund = (mask_fund == 0)\n",
    "\n",
    "mask_harm = numpy.zeros((data_len,))\n",
    "mask_harm[[int(geom.z) for geom in harm_points.geoms]] = 1\n",
    "mask_harm = (mask_harm == 0)\n",
    "\n",
    "mask_both = mask_fund & mask_harm \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "893d5369",
   "metadata": {},
   "source": [
    "### Plotting the data\n",
    "The first plot is the loaded data, with the two feature geometries overlaid. The second plot displays the filtered data (with the two sub-features). The last two plots are histogrammes of the values of _v_ for each sub-feature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4af22d6d",
   "metadata": {},
   "outputs": [],
   "source": [
    "t = dataset['time']['center'][...].value\n",
    "f = dataset['frequency']['center'][...].value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3c81a28",
   "metadata": {},
   "outputs": [],
   "source": [
    "v = dataset['v']['center'][...].value\n",
    "\n",
    "import matplotlib.pyplot as pyplot\n",
    "import matplotlib.colors as colors\n",
    "\n",
    "fig, ax = pyplot.subplots(figsize=(16,9))\n",
    "scaleZ = colors.Normalize(vmin=-1, vmax=1)\n",
    "ax.set_yscale('log')\n",
    "im = ax.pcolormesh(t, f, v, norm=scaleZ, cmap='gray')\n",
    "ax.plot(crs.time_converter(fund_polygon.exterior.xy[0]).datetime, fund_polygon.exterior.xy[1], 'k')\n",
    "ax.plot(crs.time_converter(harm_polygon.exterior.xy[0]).datetime, harm_polygon.exterior.xy[1], '--b', linewidth=4)\n",
    "ax.set_ylim((100, 1500))\n",
    "fig.colorbar(im, label=\"Stokes V\")\n",
    "ax.set_xlabel(\"Time\", fontsize=\"xx-large\")\n",
    "ax.set_ylabel(crs.spectral_label, fontsize=\"xx-large\")\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2387b850",
   "metadata": {},
   "outputs": [],
   "source": [
    "v = numpy.ma.masked_array(dataset['v']['center'][...].value, mask=mask_both)\n",
    "\n",
    "fig, ax = pyplot.subplots()\n",
    "scaleZ = colors.Normalize(vmin=-1.5, vmax=1.5)\n",
    "ax.set_yscale('log')\n",
    "ax.pcolormesh(t, f, v, norm=scaleZ, cmap='jet')\n",
    "ax.set_ylim((100, 1500))\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c003cda",
   "metadata": {},
   "outputs": [],
   "source": [
    "vf = numpy.ma.masked_array(dataset['v']['center'][...].value, mask=mask_fund)\n",
    "n, bins, patches = pyplot.hist(vf.compressed(), 25, (-1.2, 1.2), facecolor='g', alpha=0.75)\n",
    "pyplot.xlabel('V')\n",
    "pyplot.ylabel('Count')\n",
    "pyplot.title('Histogram of V (fundamental SKR component)')\n",
    "pyplot.xlim(-1.2, 1.2)\n",
    "pyplot.grid(True)\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "070a1294",
   "metadata": {},
   "outputs": [],
   "source": [
    "vh = numpy.ma.masked_array(dataset['v']['center'][...].value, mask=mask_both)\n",
    "n, bins, patches = pyplot.hist(vh.compressed(), 25, (-1.2, 1.2), facecolor='g', alpha=0.75)\n",
    "pyplot.xlabel('V')\n",
    "pyplot.ylabel('Count')\n",
    "pyplot.title('Histogram of V (harmonic SKR component)')\n",
    "pyplot.xlim(-1.2, 1.2)\n",
    "pyplot.grid(True)\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a550c12",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
