{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "08d6286a",
   "metadata": {},
   "source": [
    "# TFCat MultiPoint feature and feature processing\n",
    "\n",
    "## Description\n",
    "The catalogue used in the example is available from _Taubenschuss et al (2021a)_. It contains patches of Kilometric Radiation (SKR) events displaying Faraday rotation in the observed polarization. The analysis is described in _Taubenschuss et al (2021b)_. \n",
    "\n",
    "Each feature has been identified by the study authors. The geometry type of the features is _MultiPoint_, tracing the contour of the patches. \n",
    "\n",
    "In this example, we process the first feature of the catalogue and show how to process the geometries using _shapely_, for converting the _MultiPoint_ geometry into a _Polygon_. \n",
    "\n",
    "## References\n",
    "- Taubenschuss, U., B. Cecconi, L. Lamy. (2021a), Catalogue of Faraday rotation patches identified in Saturn Kilometric Radiation (SKR) observations by Cassini/RPWS/HFR (Version 1.0) [Data set]. PADC. https://doi.org/10.25935/R11G-6J63\n",
    "- Taubenschuss, U., L. Lamy, G. Fischer, D. Píša, O. Santolík, J. Souček, W. S. Kurth, B. Cecconi, P. Zarka, & H. O. Rucker (2021b). The Faraday rotation effect in Saturn Kilometric Radiation observed by the CASSINI spacecraft. Icar, 370 114661. https://doi.org/10.1016/j.icarus.2021.114661\n",
    "\n",
    "\n",
    "## Requires\n",
    "- astropy\n",
    "- numpy \n",
    "- shapely\n",
    "- matplotlib\n",
    "- TFCat "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b2c99b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "!{sys.executable} -m pip install tfcat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "939c0c02",
   "metadata": {},
   "outputs": [],
   "source": [
    "from tfcat import TFCat\n",
    "from matplotlib import pyplot as plt\n",
    "import numpy\n",
    "from shapely.geometry import shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "addaffa2",
   "metadata": {},
   "source": [
    "### Loading the catalogue and display the geometry type of the first feature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa214fd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "tf_file = '../data/cassini_faraday_patches_2006.json'\n",
    "cat = TFCat.from_file(tf_file)\n",
    "cat.feature(0).geometry.type"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20bfd7e5",
   "metadata": {},
   "source": [
    "### Displaying the MultiPoint feature\n",
    "The way the data points has been recorded are shown on the figure: dots are the recorded points, the dashed line is showing the ordering of the points. The first point is mark with a $\\triangleright$, and the last one with $\\triangleleft$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "318a0622",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting the MultiPoint feature:\n",
    "multi_point = shape(cat.feature(0).geometry)\n",
    "xs = numpy.array([point.x for point in multi_point])\n",
    "ys = numpy.array([point.y for point in multi_point])\n",
    "\n",
    "plt.plot(xs[1:-2], ys[1:-2], 'o')\n",
    "plt.plot(xs[0], ys[0], 'r>')\n",
    "plt.plot(xs[-1], ys[-1], 'r<')\n",
    "plt.plot(xs, ys, 'k--')\n",
    "plt.title(\"Feature 0 -- Unordered MultiPoint\")\n",
    "plt.xlabel(cat.crs.time_label)\n",
    "plt.ylabel(cat.crs.spectral_label)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5f3cae9",
   "metadata": {},
   "source": [
    "### Processing unordered MultiPoint to build a contour Polygon \n",
    "We fisrt use the built-in _convex_hull()_ method, and then apply a more detailed process based on the _buffer()_ method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6fc26710",
   "metadata": {},
   "outputs": [],
   "source": [
    "from shapely.geometry import shape\n",
    "from shapely.geometry.polygon import LineString\n",
    "\n",
    "# Plotting the MultiPoint feature:\n",
    "multi_point = shape(cat.feature(0).geometry)\n",
    "xs = numpy.array([point.x for point in multi_point])\n",
    "ys = numpy.array([point.y for point in multi_point])\n",
    "\n",
    "plt.plot(xs, ys, '+')\n",
    "plt.title(\"Feature 0 -- convex_hull()\")\n",
    "plt.xlabel(cat.crs.time_label)\n",
    "plt.ylabel(cat.crs.spectral_label)\n",
    "\n",
    "# computing the convex hull circling the MultiPoint feature  \n",
    "hull = multi_point.convex_hull\n",
    "xh, yh = hull.exterior.xy\n",
    "\n",
    "# Plot the convex hull result and the buffer method \n",
    "plt.plot(xh, yh, 'y--')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9de6bb48",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Using Shapely buffer() method to obtain a refine exterior Polygon\n",
    "# NB: works better on normalized coordinates\n",
    "\n",
    "plt.plot(xs, ys, '+')\n",
    "plt.title(\"Feature 0 -- buffer()\")\n",
    "plt.xlabel(cat.crs.time_label)\n",
    "plt.ylabel(cat.crs.spectral_label)\n",
    "\n",
    "\n",
    "xx = (xs - min(xs))/(max(xs) - min(xs))\n",
    "yy = (ys - min(ys))/(max(ys) - min(ys))\n",
    "# Create a LineString object to use the buffer() method\n",
    "lines = LineString(list(zip(xx, yy)))\n",
    "# Apply the buffer() method (more details in the Shapely documentation)\n",
    "xb, yb = lines.buffer(0.1, cap_style=3).buffer(-0.1, join_style=3).exterior.xy\n",
    "# Convert back to real coordinates\n",
    "xb = numpy.array(xb) * (max(xs) - min(xs)) + min(xs)\n",
    "yb = numpy.array(yb) * (max(ys) - min(ys)) + min(ys)\n",
    "\n",
    "# Plot the convex hull result and the buffer method \n",
    "plt.plot(xb, yb, 'y--')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2323aef",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
